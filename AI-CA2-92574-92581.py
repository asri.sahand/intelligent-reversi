
## Display Board

# Outside edge -> ?, empty_squares -> ., black -> @, and white -> O.
BLACK, WHITE, EMPTY, OUT = '@', 'O', '.', '?'
PLAYERS = {BLACK: 'Black', WHITE: 'White'}

# Add a direction to a square to refer to neighbor squares.
UP, DOWN, RIGHT, LEFT = -10, 10, 1, -1
UP_RIGHT, DOWN_RIGHT, DOWN_LEFT, UP_LEFT = -9, 11, 9, -11
DIRECTIONS = (UP, DOWN, RIGHT, LEFT, UP_RIGHT, DOWN_RIGHT, DOWN_LEFT, UP_LEFT)

# This function gives all the valid squares
def squares():
	return [i for i in xrange(11, 89) if 1 <= (i % 10) <= 8]

# To initialize a new board in initial-state
def init_board():
	board = [OUT] * 100
	for i in squares():
		board[i] = EMPTY
	# The middle four squares should hold the initial piece positions.
	board[44], board[45] = WHITE, BLACK
	board[54], board[55] = BLACK, WHITE
	return board

# Convert board in list form to board in string form
def print_board(board):
	board_in_str = ''
	board_in_str += '  %s\n' % ' '.join(map(str, range(1, 9)))
	for row in xrange(1, 9):
		begin, end = 10*row + 1, 10*row + 9
		board_in_str += '%d %s\n' % (row, ' '.join(board[begin:end]))
	return board_in_str


## How to Make Move

# Does move refer to a valid square
def is_move_valid(move):
	return isinstance(move, int) and move in squares()

# Get player's opponent
def opponent(player):
	return BLACK if player is WHITE else WHITE

# Find a square which makes a bracket with `square` for `player` in the
# `direction`.  If no such square exists it returns None .
def find_board_bracket(square, player, board, direction):
	bracket = square + direction
	if board[bracket] == player:
		return None
	opp = opponent(player)
	while board[bracket] == opp:
		bracket += direction
	return None if board[bracket] in (OUT, EMPTY) else bracket

# Does move form a bracket with another piece of the same color with pieces of the
# opposite color in between
def is_move_legal(move, player, board):
	"""Is this a legal move for the player?"""
	hasbracket = lambda direction: find_board_bracket(move, player, board, direction)
	return board[move] == EMPTY and any(map(hasbracket, DIRECTIONS))

## Make moves

# This reflects the move on the board
def reflect_move(move, player, board):
	board[move] = player
	for d in DIRECTIONS:
		apply_flips(move, player, board, d)
	return board

# Apply flips as the result of given move
def apply_flips(move, player, board, direction):
	bracket = find_board_bracket(move, player, board, direction)
	if not bracket:
		return
	square = move + direction
	while square != bracket:
		board[square] = player
		square += direction

## Monitor players' moves

class IllegalMoveException(Exception):
	def __init__(self, player, move, board):
		self.player = player
		self.move = move
		self.board = board

	def __str__(self):
		return '%s cannot move to square: %d' % (PLAYERS[self.player], self.move)

def get_all_legal_moves(player, board):
	return [sq for sq in squares() if is_move_legal(sq, player, board)]

def get_one_legal_move(player, board):
	return any(is_move_legal(sq, player, board) for sq in squares())

## How to Play Game

# Play the game and return the final board state and score
def play(black_strategy, white_strategy):
	board = init_board()
	player = BLACK
	strategy = lambda who: black_strategy if who == BLACK else white_strategy
	while player is not None:
		move = get_move(strategy(player), player, board)
		reflect_move(move, player, board)
		player = get_next_player(board, player)
	return board, compute_score(BLACK, board)

# Determine the next player for next step, if no legal moves exist, returns None
def get_next_player(board, prev_player):
	opp = opponent(prev_player)
	if get_one_legal_move(opp, board):
		return opp
	elif get_one_legal_move(prev_player, board):
		return prev_player
	return None

# Get a move based on 'strategy'
def get_move(strategy, player, board):
	copy = list(board) # copy this board to prevent cheating :D
	move = strategy(player, copy)
	if not is_move_valid(move) or not is_move_legal(move, player, board):
		raise IllegalMoveException(player, move, copy)
	return move

# Compute score of 'player' minus opponent's score
def compute_score(player, board):
	mine, theirs = 0, 0
	opp = opponent(player)
	for sq in squares():
		piece = board[sq]
		if piece == player: mine += 1
		elif piece == opp: theirs += 1
	return mine - theirs

#	http://www.samsoft.org.uk/reversi/strategy.htm

SQUARES_WEIGHTS = [
	0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
	0,  99,  -8,   8,   6,   6,   8,  -8,  99,   0,
	0,  -8, -24,  -4,  -3,  -3,  -4, -24,  -8,   0,
	0,   8,  -4,   7,   4,   4,   7,  -4,   8,   0,
	0,   6,  -3,   4,   0,   0,   4,  -3,   6,   0,
	0,   6,  -3,   4,   0,   0,   4,  -3,   6,   0,
	0,   8,  -4,   7,   4,   4,   7,  -4,   8,   0,
	0,  -8, -24,  -4,  -3,  -3,  -4, -24,  -8,   0,
	0,  99,  -8,   8,   6,   6,   8,  -8,  99,   0,
	0,   0,   0,   0,   0,   0,   0,   0,   0,   0,
]

def add_score(score , sq):
	return score + SQUARES_WEIGHTS[sq]
def another_compute_score(player, board):
	mine , theirs = 0 , 0
	opp = opponent(player)
	for sq in squares():
		piece = board[sq]
		if piece == player :
			mine = add_score(mine , sq)
		elif piece == opp:
			theirs = add_score(theirs , sq)
	return mine - theirs

## Game Strategies

### Random

# The easiest strategy, chooses a move randomly.
import random

# Chooses a random legal move
def random_strategy(player, board):
	return random.choice(get_all_legal_moves(player, board))

### Minimax search

# Find the best legal move for 'player', and searching to the given depth.
# Returns a tuple (move, min_score), where min_score is the minimum
# score achievable for player if the move is made.








# def minimax(player, board, depth, evaluate):
#     # Define the value of the board to be the opposite of its value to our
#     # opponent, computed by applying `minimax` recursively for our opponent.
#     def value(board):
#         return -minimax(opponent(player), board, depth-1, evaluate)[0]

#     # When depth becomes zero, don't examine more possible moves, determine the value
#     # of the board to the 'player'.
#     if depth == 0:
#         return evaluate(player, board), None

#     # Evaluate all of the legal moves by considering their implications
#     # `depth` turns in advance.
#     moves = get_all_legal_moves(player, board)

#     # when player has no legal moves
#     if not moves:
#         # the game is over, so the best achievable score is victory or loss
#         if not get_one_legal_move(opponent(player), board):
#             return final_value(player, board), None
#         # or pass this turn, so just find the value of the board.
#         return value(board), None

#     # If there are multiple legal moves, choose the best one by
#     # maximizing the value of the resulting boards.

#     v = value(reflect_move(moves[0], player, list(board)))
#     first_idx = True
#     best_move = moves[0]
#     for m in moves:
#     	if first_idx:
#     		continue
#     		first_idx = False
#     	temp_v = value(reflect_move(m, player, list(board)))
#         if v < temp_v:
#         	best_move = m
#         	v = temp_v

#     return v, best_move
#     # v = max((value(reflect_move(m, player, list(board))), m) for m in moves)
#     # print v
#     return v 
# # Big constants for endgame boards.




def minimax(player, board, depth, evaluate):
    # Define the value of the board to be the opposite of its value to our
    # opponent, computed by applying `minimax` recursively for our opponent.
    def value(board):
        return -minimax(opponent(player), board, depth-1, evaluate)[0]

    # When depth becomes zero, don't examine more possible moves, determine the value
    # of the board to the 'player'.
    if depth == 0:
        return evaluate(player, board), None

    # Evaluate all of the legal moves by considering their implications
    # `depth` turns in advance.
    moves = get_all_legal_moves(player, board)

    # when player has no legal moves
    if not moves:
        # the game is over, so the best achievable score is victory or loss
        if not get_one_legal_move(opponent(player), board):
            return final_value(player, board), None
        # or pass this turn, so just find the value of the board.
        return value(board), None

    # If there are multiple legal moves, choose the best one by
    # maximizing the value of the resulting boards.
    return max((value(reflect_move(m, player, list(board))), m) for m in moves)


MAX_VALUE = sum(map(abs, SQUARES_WEIGHTS))
MIN_VALUE = -MAX_VALUE

# When the game is over
def final_value(player, board):
	diff = compute_score(player, board)
	if diff < 0:
		return MIN_VALUE
	elif diff > 0:
		return MAX_VALUE
	return diff

# Make a strategy which uses `minimax` with the specified leaf board
# and evaluation function.
def minimax_search(depth, evaluate):
	def strategy(player, board):
		return minimax(player, board, depth, evaluate)[1]
	return strategy

### Alpha-Beta search

def alphabeta(player, board, depth, evaluate, alpha, beta , max_player):

	def value(board):
		return alphabeta(opponent(player), board, depth-1, evaluate, alpha, beta, not max_player)[0]
	if depth == 0:
		if max_player:
			return evaluate(player, board), None
		else:
			return -evaluate(player, board), None
	moves = get_all_legal_moves(player, board)
	if not moves:
		if not get_one_legal_move(opponent(player), board):
			return final_value(player, board), None
		return value(board), None

	best_move = None
	if max_player:
		val = MIN_VALUE
		for move in moves:
			temp_val = value(reflect_move(move , player , list(board)))
			if val < temp_val:
				best_move = move
				val = temp_val
			alpha = max(val , alpha)
			if alpha >= beta:
				break
	else:
		val = MAX_VALUE
		for move in moves:
			temp_val = value(reflect_move(move , player , list(board)))
			if val < temp_val:
				best_move = move
				val = temp_val
			beta = min(val , beta)
			if alpha >= beta:
				break
	return val, best_move


# def alphabeta(player, board, depth, evaluate, alpha, beta, maximizing_player):
# 	# Define the value of the board to be the opposite of its value to our
# 	# opponent, computed by applying `minimax` recursively for our opponent.
# 	def value(board):
# 		return alphabeta(opponent(player), board, depth-1, evaluate, alpha, beta, not maximizing_player)[0]

# 	# When depth becomes zero, don't examine more possible moves, determine the value
# 	# of the board2 to the 'player'.
# 	if depth == 0:
# 		return evaluate(player, board), None
# 		# if maximizing_player:
# 		# 	return evaluate(player, board), None
# 		# else:
# 		# 	return -evaluate(player, board), None

# 	# Evaluate all of the legal moves by considering their implications
# 	# `depth` turns in advance.
# 	moves = get_all_legal_moves(player, board)


# 	# when player has no legal moves
# 	if not moves:
# 		# the game is over, so the best achievable score is victory or loss
# 		if not get_one_legal_move(opponent(player), board):
# 			return final_value(player, board), None
# 		# or pass this turn, so just find the value of the board.
# 		return value(board), None

# 	if maximizing_player:		
# 		for m in moves:
# 			val = max((alphabeta(opponent(player), board, depth-1, evaluate, alpha, beta, False)[0],m))
# 			alpha = max(alpha, val)
# 			if beta <= alpha:
# 				break
# 		return (alpha,m)
# 	else:
# 		for m in moves:
# 			val = min((alphabeta(opponent(player), board, depth-1, evaluate, alpha, beta, True)[0], m))
# 			beta = min(beta, val)
# 			if beta <= alpha:
# 				break
# 		return (beta,m)

# 	# If there are multiple legal moves, choose the best one by
# 	# maximizing the value of the resulting boards.
	

	
def alphabeta_search(depth, evaluate):
	def strategy(player, board):
		return alphabeta(player, board, depth, evaluate, MIN_VALUE, MAX_VALUE, True)[1]
	return strategy

# If you want to choose play yourself
def human(player, board):
	print print_board(board)
	print 'Your move?'
	while True:
		move = raw_input('> ')
		if move and check_move(int(move), player, board):
			return int(move)
		elif move:
			print 'Illegal move--try again.'

## Running the Game
def check_move(move, player, board):
	return is_move_valid(move) and is_move_legal(move, player, board)

def choose_algorithm(prompt, algorithms):
	print prompt
	print 'Options:', algorithms.keys()
	while True:
		choice = raw_input('>> ')
		if choice in algorithms:
			return algorithms[choice]
		elif choice:
			print 'Invalid choice--try again. '

def set_players():
	print 'Hello!'
	algorithms = { 'human': human,
				'random': random_strategy,
				'minimax': minimax_search(3, compute_score),
				'smarter-minimax':
					minimax_search(3, another_compute_score),
				'alpha-beta': alphabeta_search(3, compute_score),
				'smarter-alpha-beta':
					alphabeta_search(3, another_compute_score) }
	black = choose_algorithm('BLACK: please choose a strategy', algorithms)
	white = choose_algorithm('WHITE: please choose a strategy', algorithms)
	return black, white

def main():
	try:
		black, white = set_players()
		board, score = play(black, white)
	except IllegalMoveException as e:
		print e
		return
	except EOFError as e:
		print 'Goodbye. :-)'
		return
	print '%s wins!' % ('Black' if score > 0 else 'White')
	print 'score:', score
	print print_board(board)

main()
